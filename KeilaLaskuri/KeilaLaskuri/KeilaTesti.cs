﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace KeilaLaskuri
{
    [TestFixture]
    public class KeilaTesti
    {
        Game bowling;

        [SetUp]
        public void setup()
        {
            bowling = new Game();
        }

        [Test]
        public void heittotesti()
        {
            bowling.Heitä(5);
            Assert.AreEqual(5, bowling.Pistelasku());
        }

        [Test]
        public void KaatoTesti()
        {
            bowling.Heitä(10);
            bowling.Heitä(5);
            bowling.Heitä(4);
            Assert.AreEqual(28, bowling.Pistelasku());
        }

        [Test]
        public void KaikkiKaatojaTesti()
        {
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);
            Assert.AreEqual(300, bowling.Pistelasku());
        }

        [Test]
        public void PaikkoTesti()
        {
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(6);
            Assert.AreEqual(22, bowling.Pistelasku());
        }

        [Test]
        public void KaikkiHuti()
        {
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);

            Assert.AreEqual(0, bowling.Pistelasku());
        }

        [Test]
        public void KunnonPeli()
        {
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(4);
            bowling.Heitä(3);
            bowling.Heitä(6);
            bowling.Heitä(4);
            bowling.Heitä(9);
            bowling.Heitä(1);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(1);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(0);
            bowling.Heitä(5);
            bowling.Heitä(3);
            bowling.Heitä(8);
            bowling.Heitä(2);
            bowling.Heitä(9);

            Assert.AreEqual(120, bowling.Pistelasku());
        }

        [Test]
        public void ViimeisetHeitotKaatoja()
        {
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(10);

            Assert.AreEqual(84, bowling.Pistelasku());
        }

        [Test]
        public void KaikkiPaikkoja()
        {
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);
            bowling.Heitä(5);

            Assert.AreEqual(150, bowling.Pistelasku());
        }

        [Test]
        public void VikaheittoTesti()
        {
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(5);
            bowling.Heitä(3); //Pistelasku loppuu tähän
            bowling.Heitä(5); //Tätä ei lasketa koska edellinen ei ollut paikko tai kaato

            Assert.AreEqual(62, bowling.Pistelasku());
        }

        [Test]
        public void VikaheittoKaato()
        {
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(8);
            bowling.Heitä(2);
            bowling.Heitä(10);

            Assert.AreEqual(74, bowling.Pistelasku());
        }

        [Test]
        public void VikaRuutuKaksiKaatoa()
        {
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(3);
            bowling.Heitä(10);
            bowling.Heitä(10);
            bowling.Heitä(5);

            Assert.AreEqual(79, bowling.Pistelasku());
        }

        }
}
