﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Piste lasku logiikkaan otettiin apua youtube videosta: https://www.youtube.com/watch?v=8Tdo8fslDsQ 
//Kaikki muut on tehty itse

namespace KeilaLaskuri
{
    public class Game
    {
        public bool testing = false;

        int pisteet = 0;
        int heittoMäärä = 0;
        int[] heitot = new int[21];

        //Listat johon tulostetaan loppu pisteet ja välivaiheet
        public List<object> taulukko1 = new List<object>();
        public List<object> taulukko2 = new List<object>();
        public List<object> taulukko3 = new List<object>();

        public int ekaheitto = 0;
        public int tokaheitto = 0;

        //Lisää kaadettujen keilan määrän pistelaskuun
        //(Tätä kutsutaan kun käyttäjä antaa luvun)
        public void Heitä(int keilat)
        {
            heitot[heittoMäärä] = keilat;
            heittoMäärä++;
        }

        //Tämä vasta kutsutaan lopussa ja se laskee kaikkien heittojen pisteet
        public int Pistelasku()
        {
            heittoMäärä = 0;

            for (int ruutu = 0; ruutu < 10; ruutu++)
            {
                //Estää kaatumisen. Käyttäjä ei mene taulukon ulkopuolelle
                if (heittoMäärä + 2 >= heitot.Length)
                    return pisteet;

                if (Kaato())
                {
                    pisteet += Kaadonpisteet();
                    heittoMäärä++;
                }
                else if (Paikko())
                {
                    pisteet += Paikonpisteet();
                    heittoMäärä += 2;
                }
                else
                {
                    pisteet += Ruudunpisteet();
                    heittoMäärä += 2;
                }
            }
            return pisteet;
        }

        bool Kaato()
        {
            return heitot[heittoMäärä] == 10;
        }
        //Kaadossa lasketaan kahden seuraavan heiton tulokset nykyiseen ruutuun
        int Kaadonpisteet()
        {
            return heitot[heittoMäärä] + heitot[heittoMäärä + 1] + heitot[heittoMäärä + 2];
        }

        bool Paikko()
        {
            return heitot[heittoMäärä] + heitot[heittoMäärä + 1] == 10;
        }
        //Paikossa lasketaan seuraava heitto nykyisen ruudun tulokseen
        int Paikonpisteet()
        {
            return heitot[heittoMäärä] + heitot[heittoMäärä + 1] + heitot[heittoMäärä + 2];
        }
        //Normaali ruutu ilman kaatoa/paikkoa
        int Ruudunpisteet()
        {
            return heitot[heittoMäärä] + heitot[heittoMäärä + 1];
        }

        bool viimeinen(int vuoro)
        {
            return vuoro == 10;
        }

        //Kysytään käyttäjältä ensimmäinen heitto
        void kysy1(int vuoro)
        {
            Console.Clear();

            if (!testing)
            {
                Console.WriteLine("Frame: " + vuoro + "\n" + "Anna ensimmäinen piste!");
                string s = Console.ReadLine();
                int.TryParse(s, out ekaheitto);
            }

            //Jos käyttäjä antaa yli kymmenen tai alle 0 niin se pyöristetään sallittuun numeroon
            if (ekaheitto > 10)
                ekaheitto = 10;
            if (ekaheitto < 0)
                ekaheitto = 0;
        }
        //Kysytään käyttäjältä toinen heitto
        void kysy2(int vuoro)
        {
            Console.Clear();

            if (!testing)
            {
                Console.WriteLine("Frame: " + vuoro + "\n" + "Anna toinen piste!");
                string s = Console.ReadLine();
                int.TryParse(s, out tokaheitto);
            }

            if (tokaheitto < 0)
                tokaheitto = 0;

            if (ekaheitto + tokaheitto > 10)
                tokaheitto = 10 - ekaheitto;
        }
        //Aloitetaan Console ohjelma
        public void UserInput()
        {

            int vuoro = 1;

            //Looppi kaikille heitoille
            for (int i = 0; i < heitot.Length; i++)
            {

                ekaheitto = 0;
                tokaheitto = 0;

                //Kysytään ensimmäinen heitto ja asetetaan vuoro
                kysy1(vuoro);
                    if (i >= heitot.Length - 1)
                    {

                        Heitä(ekaheitto);
                    }
                    else
                    {

                        Heitä(ekaheitto);

                        if (ekaheitto != 10)
                        {
                            kysy2(vuoro);
                            Heitä(tokaheitto);
                        }

                    }
                
                // Tämä ei vaikuta pistelaskuun, vaan siihen miltä se näyttää konsolissa
                // Taulukoi ekan heiton tulokset!
                if (ekaheitto == 10)
                {
                    taulukko2.Add("X");
                    if (!viimeinen(vuoro))
                    {
                        taulukko2.Add(" ");
                        i++;
                    }

                }
                else if (ekaheitto == 0)
                {
                    taulukko2.Add("-");
                }
                else if (ekaheitto != 0)
                {
                    taulukko2.Add(ekaheitto);
                }

                // Taulukoi tokan heiton tulokset
                if (tokaheitto == 10)
                {
                    taulukko2.Add("/");
                }
                else if (tokaheitto + ekaheitto == 10 && ekaheitto != 10)
                {
                    taulukko2.Add("/");
                }
                else if (tokaheitto == 0 && ekaheitto != 10)
                {
                    taulukko2.Add("-");
                }
                else if (tokaheitto != 0 && tokaheitto != 10)
                {
                    taulukko2.Add(tokaheitto);
                }

                if (vuoro < 11)
                {
                    taulukko1.Add("   " + (vuoro));
                    taulukko1.Add("  ");
                    vuoro++;
                }

                if (ekaheitto != 10)
                    i++;
            }
            //Viimeisessä ruudussa jos on kaksi kaatoa niin antaa heittää vielä yhden heiton
            if (heitot[heittoMäärä - 2] == 10 && heitot[heittoMäärä - 1] == 10)
            {
                kysy1(12);
                Heitä(ekaheitto);
            }


            taulukko1.Add("" + ("Score"));
            taulukko1.Add("  ");

            //Lisätään loppupisteet taulukko2:seen
            pisteet = Pistelasku();
            taulukko2.Add(pisteet);

            print();
        }

        //Printataan taulukot
        public void print()
        {
            int count = 0;
            string spacer = "";

            taulukko2.RemoveAt(taulukko2.Count - 2);

            //Näyttää ruudun numerot
            //Spacer tulostaa välin jos count on 0
            for (int j = 0; j < taulukko1.Count; j++)
            {
                if (count > 0) { count = 0; spacer = "   "; } else { count++; spacer = ""; }
                Console.Write(taulukko1[j] + spacer);
            }

            Console.WriteLine("\n");
            //Näyttää kaikki heitot
            for (int j = 0; j < taulukko2.Count; j++)
            {
                if (count > 0) { count = 0; spacer = "   "; } else { count++; spacer = ""; }
                Console.Write("[" + taulukko2[j] + "]" + spacer);
            }

            Console.WriteLine("\n");
            Console.WriteLine("\n");

            //Tämä on vain testausta varten. Tulostaa heitot arrayn
            //for (int i = 0; i < heitot.Length; i++)
            //{
            //    if (count > 0) { count = 0; spacer = "   "; } else { count++; spacer = ""; }
            //    Console.Write("[" + heitot[i] + "]" + spacer);
            //}

        }

        class Program
        {
            public static void Main()
            {
                Game game = new Game();

                game.UserInput();
            }
        }
    }
}